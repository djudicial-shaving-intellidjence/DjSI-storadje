#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""@file   setup.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   05 Mar 2022

@brief DjSI_storadje is a library to cache, combine, and analyse reddit and
       pushshift data. It is part of the Djudicial Shaving Intellidjence 
       Adjency

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""


from setuptools import setup, find_packages

setup(name="DjSI_storadje",
      packages=find_packages(),
      author="Djundjila",
      author_email="djundjila.gitlab@cloudmail.altermail.ch",
      test_suite="tests",
      entry_points={"console_scripts":
                    ["quick_build=bin.quick_build:main",
                     "refresh_post=bin.refresh_post:main",
                     "eval_tags=bin.eval_tags:main",
                     "find_AA_post=bin.find_AA_post:main"]}
      )
