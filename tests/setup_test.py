#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   setup_tests.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   05 Mar 2022

@brief  these tests just check the directory structure

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import unittest

class Test(unittest.TestCase):
    def test_nothing(self):
        self.assertTrue(True)

if __name__ == "__main__":
    unittest.main()
