#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   raw_pushshift_records_test.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   06 Mar 2022

@brief  tests for the raw pushshift record storage

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from DjSI_storadje import PushShiftComment, PushShiftPost
from DjSI_storadje import StoradjeBase


class TestPost(unittest.TestCase):
    def setUp(self):
        echo = False  # very noisy if True
        if os.getenv("RUN_IN_CI") == '1':
            user = "postgres"
            password = ""
            db = 'djsia_ci_test_space'
            self.engine = create_engine(
                f'postgresql://{user}:{password}@postgres:5432/{db}',
                echo=echo)
        else:
            user = 'djsia_ci_user'
            password = 'dummy'
            db = 'djsia_ci_test_space'
            self.engine = create_engine(
                f'postgresql://{user}:{password}@localhost/{db}',
                echo=echo)
        StoradjeBase.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)()

    def tearDown(self):
        self.session.rollback()
        StoradjeBase.metadata.drop_all(self.engine)

    def test_construction(self):
        post1 = PushShiftPost(rid="abcde", data="{id='abcde'}",
                              subreddit="sub")
        comment1 = PushShiftComment(rid="fghij", data="{id='fghij'}",
                                    subreddit="sub")

        self.session.add_all((post1, comment1))
        self.session.commit()
        print(post1)


if __name__ == "__main__":
    unittest.main()
