#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   eval_tags.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Aug 2022

@brief  Script for checking which comments in a post contain a certain string

Copyright © 2022 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import praw
import DjSI_storadje as Djs

import logging
import sys

from sqlalchemy import func

from datetime import datetime

from .quick_build import get_session
import argparse


def parse_args():

    parser = argparse.ArgumentParser(
        description=("check whether any comments in a post contain a certain "
                     "string"))
    parser.add_argument("db_config", type=str,
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("--chunksize", "-n", type=int, default=1000,
                        help=("how many chunks to treat at once, must be "
                              "between 1 and 1000"))

    parser.add_argument("logfile", type=str,
                        help="path to a logfile")
    parser.add_argument("post_rid", type=str,
                        help="reddit id of the post to check")
    parser.add_argument("search_string", type=str,
                        help="string to search for")
    parser.add_argument("output_file", type=str,
                        help="file to write report to")
    parser.add_argument("deadline", type=datetime.fromisoformat,
                        help="deadline before considered late")

    args = parser.parse_args()
    args.commit=False
    if not (1 <= args.chunksize <= 1000):
        parser.error(
            f"Chunksize must be between 1 and 1000, {args.chunksize} isnt.")

    return args


def main():
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,
        handlers=(logging.FileHandler(args.logfile),
                  logging.StreamHandler(sys.stdout)),
        format='%(asctime)s %(levelname)s:%(name)s:%(message)s')

    logging.info(f"started with cli-args '{args}'")
    logger = logging.getLogger('psaw')
    logger.setLevel(logging.WARNING)

    session_manager = get_session(args)

    return work(session_manager, args.post_rid, args.search_string,
                args.deadline)


def work(session_manager, post_rid, search_string, deadline):
    with session_manager.transaction():
        post_pk,  = session_manager.query(Djs.Post.post_pk).filter_by(
            rid=post_rid).one_or_none()
        if post_pk is None:
            logging.exception(f"failed to fetch post '{post_rid}'")
            return 1
        print(f"Post_PK = {post_pk}")
        subquery = session_manager.query(
            func.max(Djs.CommentBody.retrieved_on),
            Djs.CommentBody.comment_pk).filter(
                func.lower(Djs.CommentBody.text).contains(search_string.lower())
            ).join(Djs.Comment).filter_by(
                level=0).join(Djs.Post).filter_by(post_pk=post_pk).group_by(
                    Djs.CommentBody.comment_pk)
        subquery = subquery.subquery()
        comments = session_manager.query(Djs.Comment).join(subquery, Djs.Comment.comment_pk == subquery.c.comment_pk)

        comments = comments.order_by(Djs.Comment.created_utc)
        print(comments.count())
        logging.info(f"found {comments.count()} comments containing the string "
                     f"'{search_string}'")
        on_time = comments.filter(Djs.Comment.created_utc <= deadline.timestamp())
        late = comments.filter(Djs.Comment.created_utc > deadline.timestamp())
        logging.info(f"of which {on_time.count()} were posted on time and "
                     f"{late.count()} were late.")
        ret_str = []
        ret_str.append("#### On Time:")
        for i, comment in enumerate(on_time, start=1):
            ret_str.append(f"{i}. u/{comment.author.username} [here]"
                               f"({comment.reddit_link})")
        if late.count():
            ret_str.append("#### Late:")
            for i, comment in enumerate(late, start=1):
                ret_str.append(f"{i}. u/{comment.author.username} [here]"
                                   f"({comment.reddit_link})")
        return "\n".join(ret_str)

if __name__ == "__main__":
    main()
