#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   pushshift_size_estimate.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   06 Mar 2022

@brief  simple script to help estimate the size of the database to store all
        of r/wetshaving

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import psaw
import json

from datetime import datetime, timedelta

from configparser import ConfigParser
import os

from DjSI_storadje import (PushShiftComment, PushShiftPost, initialise_base,
                           crawl_comments, crawl_posts)


def main():
    parser = ConfigParser()
    parser.read(os.path.expanduser("~/.config/djsia.ini"))
    print(parser.sections())
    section = parser['default']
    password = section['password']
    user = section['user']
    host = section['host']
    db = section['db']
    engine = create_engine(
        f"postgresql://{user}:{password}@{host}/{db}", echo=True)
    # engine = create_engine('sqlite:////tmp/testbase.db', echo=True)
    initialise_base(engine)
    session = sessionmaker(bind=engine)()

    api = psaw.PushshiftAPI()

    now = datetime.now()
    duration = timedelta(days=365.25)
    before = int(now.timestamp())
    after = int((now-duration).timestamp())

    comments = crawl_comments(api, before=before, after=after,
                              subreddit='wetshaving', chunksize=200,
                              session=session)
    posts = crawl_posts(api, before=before, after=after,
                        subreddit='wetshaving', chunksize=50, session=session)

    existing_comments = set((s[0] for s
                             in session.query(PushShiftComment.rid)))
    session.add_all(
        (PushShiftComment(rid=comment.id, data=json.dumps(comment.d_))
         for comment in comments if comment.id not in existing_comments))
    existing_posts = set((s[0] for s in session.query(PushShiftPost.rid)))
    session.add_all((PushShiftPost(rid=post.id, data=json.dumps(post.d_))
                     for post in posts if post.id not in existing_posts))

    session.commit()


if __name__ == "__main__":
    main()
