#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   quick_build.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   08 Apr 2022

@brief  (initialise and) update the database for a list of subreddits.

Copyright © 2022 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import psaw
import praw
import DjSI_storadje as djs
from DjSI_storadje import PushShiftComment, PushShiftPost
from DjSI_storadje import (User, Subreddit, Post, Comment, sanitise_id,
                           PostFlair, CommentBody, PostBody)
from DjSI_storadje import CommentIncomplete, PostIncomplete

from DjSI_storadje import utils
import itertools

from sqlalchemy import select, update, func, or_
from sqlalchemy.orm import aliased
from sqlalchemy.exc import IntegrityError

from datetime import datetime
from collections import namedtuple

import json
import argparse
import logging
import sys


class SessionManager():
    class Transaction():
        def __init__(self, session, do_commit):
            self.__session = session
            self.__do_commit = do_commit

        def __enter__(self):
            self.__session.begin()

        def __exit__(self, type, value, traceback):
            if not self.__do_commit:
                self.__session.rollback()
            else:
                self.__session.commit()

    def __init__(self, session, do_commit):
        self.__session = session
        self.__do_commit = do_commit

    @property
    def dirty(self):
        return self.__session.dirty

    def add(self, *args, **kwargs):
        return self.__session.add(*args, **kwargs)

    def add_all(self, *args, **kwargs):
        return self.__session.add_all(*args, **kwargs)

    def query(self, *args, **kwargs):
        return self.__session.query(*args, **kwargs)

    def flush(self):
        return self.__session.flush()

    def execute(self, *args, **kwargs):
        return self.__session.execute(*args, **kwargs)

    def transaction(self):
        return self.Transaction(self.__session, self.__do_commit)

    def possibly_commit(self):
        if self.__do_commit:
            self.__session.commit()


def parse_args():
    parser = argparse.ArgumentParser(
        description=(
            "update (or initialise and update) the database for a "
            "subreddit. Update a scrape of all comments and posts off a "
            "subreddit using pushshift and reddit where pushshift "
            "has gaps. Not suitable for bulk loading!"))
    parser.add_argument("db_config", type=str,
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("praw_config", type=str,
                        help="name of the config session in your praw.ini")
    parser.add_argument("--chunksize", "-n", type=int, default=1000,
                        help=("how many chunks to treat at once, must be "
                              "between 1 and 1000"))
    parser.add_argument("--time_overlap", type=float, default=60.,
                        help=("start looking TIME_OVERLAP minutes before, to "
                              "catch newly added older posts or comments"))
    parser.add_argument("logfile", type=str,
                        help="path to a logfile")
    parser.add_argument("--commit", action='store_true',
                        help=("do commit these updates to the database "
                              "(default is just a dry run)"))
    parser.add_argument("subreddits", type=str, nargs='+',
                        help="name of the subreddits to scrape")
    args = parser.parse_args()
    if not (1 <= args.chunksize <= 1000):
        parser.error(
            f"Chunksize must be between 1 and 1000, {args.chunksize} isnt.")
    if args.time_overlap <= 0:
        parser.error("overlap time must be a positive number of minutes")
    return args


def get_session(args):
    try:
        dummy, session = utils.get_session(args.db_config)
        logging.info("fetched the connection engine")
        return SessionManager(session, args.commit)
    except Exception as err:
        logging.exception("failed to get a db session")
        raise err


def get_newest(ORM_type, session_manager, subreddit):
    with session_manager.transaction():
        timestamp = session_manager.query(
            func.max(ORM_type.created_utc)).filter(
                func.lower(ORM_type.subreddit) ==
                subreddit.display_name.lower()).scalar()
    if timestamp is None:
        logging.info(
            f"Empty table for {ORM_type.__tablename__}, this is a fresh scrape")
        timestamp = 0
    else:
        date_str = datetime.fromtimestamp(timestamp).isoformat(
            sep=' ', timespec='seconds')
        logging.info(f"newest entry in {ORM_type.__tablename__} is "
                     f"from {date_str}.")
    return timestamp


def get_stream(api, before, after, args, ORM_type, subreddit):
    data_stream = ORM_type.crawl(api=api, before=before, after=after,
                                 limit=float("inf"),
                                 subreddit=subreddit.display_name,
                                 chunksize=args.chunksize,
                                 session=None)
    ORM_stream = (ORM_type.from_json(d.d_) for d in data_stream)
    logging.info(
        f"fetched data stream for {ORM_type.__tablename__}.")
    return ORM_stream


def add_pushshift_post_chunk(chunk, session_manager, args):
    rids = {c.data["id"] for c in chunk}
    with session_manager.transaction():
        already_present = {tup[0] for tup in
                           session_manager.query(PushShiftPost.rid).filter(
                               PushShiftPost.rid.in_(rids))}

    new_posts = (item for item in chunk if item.rid not in already_present)
    to_add = sorted(new_posts, key=lambda item: item.created_utc)
    nb_new_posts = len(to_add)
    with session_manager.transaction():
        session_manager.add_all(to_add)
        logging.info(
            f"added {nb_new_posts} new pushshift posts.")
    return to_add


def add_pushshift_comment_chunk(chunk, session_manager, args):
    rids = {c.data["id"] for c in chunk}
    with session_manager.transaction():
        already_present = {tup[0] for tup in
                           session_manager.query(PushShiftComment.rid).filter(
                               PushShiftComment.rid.in_(rids))}

        ## rarely, comments appear in pushshift multiple times. an example
        ## is the comment with id 'e3xmfo9'. Therefore, the already_present set
        ## needs to be updated while adding comments. ugh
        def new_comments_generator():
            for item in chunk:
                if item.rid not in already_present:
                    yield item
                    already_present.add(item.rid)
        new_comments = new_comments_generator()
        to_add = sorted(new_comments, key=lambda item: item.created_utc)
        nb_new_comments = len(to_add)

        session_manager.add_all(to_add)
        logging.info(
            f"added {nb_new_comments} new pushshift comments.")
    return to_add


def add_incomplete_posts_chunk(chunk, session_manager, reddit):
    missing_fields_posts = dict()
    incomplete_posts_to_add = list()

    # don't attempt to add any that are already present
    with session_manager.transaction():
        rids = {c.rid for c in chunk}
        exclude = {p[0] for p in session_manager.query(PostIncomplete.id).filter(
            PostIncomplete.id.in_(rids))}
        for pushshift_post in chunk:
            if isinstance(pushshift_post.data, str):
                d = json.loads(pushshift_post.data)
            elif isinstance(pushshift_post.data, dict):
                d = pushshift_post.data
            else:
                raise Exception(pushshift_post.data)
            if d["id"] in exclude:
                continue

            missing = False
            if "retrieved_on" not in d.keys():
                d["retrieved_on"] = 0
            try:
                author = d["author"] if d["author"] != '[deleted]' else None
                kwargs = dict(id=d["id"],
                              author=author,
                              created_utc=d["created_utc"],
                              title=d["title"],
                              subreddit_id=d["subreddit_id"],
                              retrieved_on=d["retrieved_on"],
                              has_pushshift=True,
                              has_reddit=False)
            except KeyError as err:
                raise KeyError(
                    f"while handling the post '{d}' caught error {err}")

            def ensure_arg(label, reddit_label=None):
                if reddit_label is None:
                    reddit_label = label
                if reddit_label in d.keys():
                    kwargs[label] = d[reddit_label]
                else:
                    global missing
                    missing=True
            ensure_arg("body", "selftext")
            ensure_arg("permalink")
            ensure_arg("link_flair_text")
            incomplete_post = PostIncomplete(**kwargs)
            incomplete_posts_to_add.append(incomplete_post)
            if missing:
                missing_fields_posts[incomplete_post.id] = incomplete_post
        logging.info(f"need to fetch {len(missing_fields_posts)} post(s) with "
                     f"missing fields.")
        info = reddit.info((f"t3_{id}" for id in missing_fields_posts.keys()))
        for rp in info:
            p = missing_fields_posts[rp.id]

            p.body = rp.selftext
            p.permalink = rp.permalink
            p.link_flair_text = rp.link_flair_text

        nb_new_incomplete_posts = len(incomplete_posts_to_add)
        session_manager.add_all(incomplete_posts_to_add)
        if incomplete_posts_to_add:
            most_recent = incomplete_posts_to_add[-1].created_utc
            most_recent = datetime.fromtimestamp(most_recent).isoformat(sep=" ")
            logging.info(
                f"added {nb_new_incomplete_posts} new incomplete posts, most "
                f"recent from {most_recent}.")
        else:
            logging.info("no new incomplete posts to add")
    return incomplete_posts_to_add


def add_incomplete_comments_chunk(chunk, session_manager, reddit):
    missing_permalink_comments = dict()
    incomplete_comments_to_add = list()
    # don't attempt to add any that are already present
    with session_manager.transaction():
        rids = {c.rid for c in chunk}
        exclude = {c[0] for c in session_manager.query(
            CommentIncomplete.id).filter(CommentIncomplete.id.in_(rids))}
        for psc in chunk:
            if isinstance(psc.data, str):
                d = json.loads(psc.data)
            elif isinstance(psc.data, dict):
                d = psc.data
            else:
                raise Exception(psc.data)

            if d["id"] in exclude:
                continue
            author = d["author"] if d["author"] != '[deleted]' else None
            kwargs = dict(id=d["id"],
                          author=author,
                          created_utc=d["created_utc"],
                          body=d["body"],
                          subreddit_id=d["subreddit_id"],
                          parent_id=d["parent_id"],
                          link_id=d["link_id"],
                          has_pushshift=True,
                          has_reddit=False)

            missing = False
            if "retrieved_on" in d.keys():
                kwargs["retrieved_on"]=d["retrieved_on"],

            if "permalink" in d.keys():
                kwargs["permalink"] = d["permalink"]
            else:
                missing = True
            incomplete_comment = CommentIncomplete(**kwargs)
            incomplete_comments_to_add.append(incomplete_comment)
            if missing:
                missing_permalink_comments[
                    incomplete_comment.id] = incomplete_comment
        if len(missing_permalink_comments):
            logging.info(
                f"need to fetch {len(missing_permalink_comments)} comments(s) "
                f"with missing permalinks.")
        info = reddit.info((f"t1_{id}" for id in
                            missing_permalink_comments.keys()))
        for rc in info:
            c = missing_permalink_comments[rc.id]
            c.permalink = rc.permalink

        nb_new_incomplete_comments = len(incomplete_comments_to_add)
        session_manager.add_all(incomplete_comments_to_add)
        if incomplete_comments_to_add:
            most_recent = incomplete_comments_to_add[-1].created_utc
            most_recent = datetime.fromtimestamp(most_recent).isoformat(sep=" ")
            logging.info(
                f"added {nb_new_incomplete_comments} new incomplete comments, "
                f"most recent from {most_recent}.")
        else:
            logging.info("no new incomplete comments to add")


    return incomplete_comments_to_add


def fix_orphan_comments(session_manager, reddit, chunksize):
    count = 0
    counter = 0
    found_missing_comments = True
    while found_missing_comments:
        with session_manager.transaction():
            existing = aliased(CommentIncomplete)
            missing_comments = session_manager.query(
                CommentIncomplete.parent_id).filter(
                    CommentIncomplete.parent_id != CommentIncomplete.link_id
                ).outerjoin(
                    existing,
                    CommentIncomplete.parent_id == "t1_" + existing.id).filter(
                        existing.id.is_(None)).order_by(
                            CommentIncomplete.parent_id,
                            CommentIncomplete.created_utc).distinct(
                                CommentIncomplete.parent_id)
            new_count = missing_comments.count()
            found_missing_comments = new_count > 0
            count += new_count

            logging.info(f"found {new_count} new missing comments "
                         "with orphan replies")
        with session_manager.transaction():
            for chunk in (list(c) for c in utils.chunkify(
                    missing_comments, chunksize)):
                this_chunksize = len(chunk)
                counter += this_chunksize
                info = reddit.info((c[0] for c in chunk))
                new_comments = list()
                for r in info:
                    author = r.author.name if r.author is not None else None
                    new_comments.append(CommentIncomplete(
                        id=r.id,
                        author=author,
                        created_utc=r.created_utc,
                        permalink=r.permalink,
                        body=r.body,
                        subreddit_id=r.subreddit_id,
                        link_id=r.link_id,
                        parent_id=r.parent_id,
                        retrieved_on=int(datetime.now().timestamp()),
                        has_pushshift=False,
                        has_reddit=True))
                session_manager.add_all(new_comments)
                session_manager.flush()
                logging.info(
                    f"added {counter}/{count} missing incomplete comments.")


def fix_missing_posts(session_manager, reddit, chunk_size):
    counter = 0
    with session_manager.transaction():
        missing_posts = session_manager.query(
            CommentIncomplete.link_id).outerjoin(
                PostIncomplete,
                CommentIncomplete.link_id == "t3_" + PostIncomplete.id).filter(
                    PostIncomplete.id.is_(None)).distinct()

        count = missing_posts.count()
        logging.info(f"found {count} missing post which appear in comments.")
        for chunk in (list(c) for c in utils.chunkify(
                missing_posts, chunk_size)):
            this_chunksize = len(chunk)

            counter += this_chunksize
            info = reddit.info((c[0] for c in chunk))
            new_posts = list()
            for r in info:
                author = r.author.name if r.author is not None else None
                new_posts.append(
                    PostIncomplete(id=r.id,
                                   author=author,
                                   created_utc=r.created_utc,
                                   permalink=r.permalink,
                                   title=r.title,
                                   body=r.selftext,
                                   subreddit_id=r.subreddit_id,
                                   retrieved_on=int(datetime.now().timestamp()),
                                   has_pushshift=False,
                                   has_reddit=True))

            session_manager.add_all(new_posts)
            logging.info(
                f"handled {counter}/{count} new posts with orphan comments.")


def fix_comment_levels(session_manager, max_level=1000):
    counter = 0
    # bootstrap level 0
    with session_manager.transaction():
        stmt = update(CommentIncomplete).where(
            CommentIncomplete.level.is_(None)).where(
                CommentIncomplete.parent_id ==
                CommentIncomplete.link_id).values(level=0)
        result = session_manager.execute(stmt)
        logging.info(f"found {result.rowcount} new comments of level 0.")

    logging.info(f"updated level zero")

    # 1 recursively update all levels
    for previous_level in range(max_level):
        new_level = previous_level + 1
        previous = aliased(CommentIncomplete)
        prev_level_ids = select(CommentIncomplete.id).filter(
            CommentIncomplete.level.is_(None)).join(
                previous, CommentIncomplete.parent_id ==
                "t1_" + previous.id).filter(
                    previous.level == previous_level)

        stmt = update(CommentIncomplete).where(
            CommentIncomplete.id == prev_level_ids.subquery().c.id).values(
                level=new_level)
        with session_manager.transaction():
            result = session_manager.execute(stmt.execution_options(
                synchronize_session=False))
            rowcount = result.rowcount
            counter += rowcount
            logging.info(f"found {rowcount} new comments of level "
                         f"{new_level}")
            remaining = session_manager.query(func.count(
                CommentIncomplete.id)).filter(
                    CommentIncomplete.level.is_(None)).scalar()
            if  remaining == 0:
                break
        logging.info(f"updated a total of {counter} comment levels so far, "
                     f"{remaining} comments left")
    # sanity check
    with session_manager.transaction():
        count = session_manager.query(func.count(CommentIncomplete.id)).filter(
            CommentIncomplete.level.is_(None)).scalar()
        if count != 0:
            raise Exception(f"I found {count} comments still without level")


def insert_subreddits(session_manager, reddit):
    with session_manager.transaction():
        needed_subreddit_ids = session_manager.query(
            PostIncomplete.subreddit_id).distinct().outerjoin(
                Subreddit,
                PostIncomplete.subreddit_id == "t5_" + Subreddit.rid).filter(
                    Subreddit.rid.is_(None))
        count = needed_subreddit_ids.count()
        if count:
            info = reddit.info((t[0] for t in needed_subreddit_ids))
            session_manager.add_all(
                (Subreddit(name=sub.display_name, rid=sub.id) for
                 sub in info))
            logging.info(f"inserted {count} subreddits")


def insert_flairs(session_manager):
    with session_manager.transaction():
        needed_flair_labels = session_manager.query(
            PostIncomplete.link_flair_text).filter(
                PostIncomplete.link_flair_text.is_not(None)).distinct(
                ).outerjoin(
                    PostFlair,
                    PostFlair.label == PostIncomplete.link_flair_text).filter(
                        PostFlair.label.is_(None))
        count = needed_flair_labels.count()
        if count:
            session_manager.add_all(
                (PostFlair(label=t[0]) for t in needed_flair_labels))
        logging.info(f"added {count} flairs")


def insert_authors(session_manager):
    with session_manager.transaction():
        authors = {t[0] for t in session_manager.query(
            CommentIncomplete.author).filter(
                CommentIncomplete.author.is_not(None)).distinct().outerjoin(
                    User, User.username == CommentIncomplete.author).filter(
                        User.username.is_(None))}
        authors |= {t[0] for t in session_manager.query(
            PostIncomplete.author).filter(
                PostIncomplete.author.is_not(None)).distinct().outerjoin(
                    User, User.username == PostIncomplete.author).filter(
                        User.username.is_(None))}
        session_manager.add_all((User(username=name) for name in authors))
    logging.info(f"found {len(authors)} individual missing authors.")


def insert_posts(session_manager):
  try:
    with session_manager.transaction():
        missing_posts = session_manager.query(PostIncomplete).outerjoin(
            Post, Post.rid == PostIncomplete.id).filter(
                Post.rid.is_(None)).order_by(PostIncomplete.created_utc)
        count = missing_posts.count()

        full_info = session_manager.query(
            PostIncomplete.created_utc.label("created_utc"),
            PostIncomplete.id.label("rid"),
            PostIncomplete.permalink.label("permalink"),
            PostIncomplete.title.label("title"),
            PostIncomplete.retrieved_on.label("retrieved_on"),
            PostIncomplete.body.label("body"),
            Subreddit.subreddit_pk.label("subreddit_pk"),
            PostIncomplete.has_pushshift.label("has_pushshift"),
            PostIncomplete.has_reddit.label("has_reddit"),
            User.user_pk.label("user_pk"),
            PostFlair.postflair_pk.label("postflair_pk")
            ).join(Subreddit,
                   PostIncomplete.subreddit_id == "t5_" + Subreddit.rid).outerjoin(
                       User,
                       User.username == PostIncomplete.author).outerjoin(
                           PostFlair,
                           PostFlair.label ==
                           PostIncomplete.link_flair_text).outerjoin(
                               Post,
                               Post.rid == PostIncomplete.id).filter(
                                   Post.rid.is_(None)).order_by(
                                       PostIncomplete.created_utc)
        full_count = full_info.count()
        row = namedtuple("row", ['created_utc', 'rid', 'permalink', 'title',
                                 'retrieved_on', 'body', 'subreddit_pk',
                                 'has_pushshift', 'has_reddit', 'user_pk',
                                 'postflair_pk'])
        if count:
            logging.info(f"Starting the insertion and indexing of {count} posts")
            chunksize = min(max(1000, count/1000), 10000)
            counter = 0
            for chunk in (list((row(*r) for r in c)) for c in utils.chunkify(
                    full_info, chunksize)):
                session_manager.add_all(
                    (Post(created_utc=r.created_utc,
                          rid=r.rid,
                          permalink=r.permalink,
                          title=r.title,
                          subreddit_pk=r.subreddit_pk,
                          has_pushshift=r.has_pushshift,
                          has_reddit=r.has_reddit,
                          user_pk=r.user_pk,
                          postflair_pk=r.postflair_pk,
                          bodies=([PostBody(retrieved_on=r.retrieved_on,
                                         title=r.title, text=r.body)]
                                if r.body is not None else [])) for
                     r in chunk))
                counter += len(chunk)
                logging.info(f"inserted and indexed {counter}/{count} posts")
  except IntegrityError as err:
      raise Exception(
          f"caught error '{err}' while dealing with post "
          f"https://reddit.com{r.permalink}")
  except Exception as err:
      raise Exception(
          f"caught error '{err}' while dealing with post "
          f"https://reddit.com{r.permalink}")


def insert_comments(session_manager, user_cache, subreddit_cache):
    with session_manager.transaction():
        missing_comments = session_manager.query(CommentIncomplete).outerjoin(
            Comment, Comment.rid == CommentIncomplete.id).filter(
                Comment.rid.is_(None))
        count = missing_comments.count()
        if count:
            logging.info(
                f"Starting the insertion and indexing of {count} comments")
            chunksize = min(max(1000, count//1000), 10000)
            counter = 0
            start = None
            for chunk in (list(c) for c in utils.chunkify(
                    missing_comments, chunksize)):
                if start is None:
                    start = datetime.now()
                session_manager.add_all(
                    (Comment.from_incomplete_comment(ip, session_manager,
                                                     user_cache,
                                                     subreddit_cache) for
                     ip in chunk))
                counter += len(chunk)
                session_manager.possibly_commit()
                elapsed = datetime.now() - start
                percentage = counter/count
                estimated_duration = elapsed/percentage
                estimated_end = (start + estimated_duration).isoformat(sep=' ')
                logging.info(
                    f"inserted and indexed {counter}/{count} "
                    f"({percentage*100:.2f}%) comments")
                logging.info(
                    f"Estimated end of insertion: {estimated_end}.")

def insert_comments_by_dependence(session_manager):
    parentComment = aliased(Comment)
    full_info = session_manager.query(
        CommentIncomplete.created_utc,
        CommentIncomplete.id,
        CommentIncomplete.permalink,
        Subreddit.subreddit_pk,
        Post.post_pk,
        CommentIncomplete.parent_id,
        parentComment.comment_pk,
        CommentIncomplete.level,
        CommentIncomplete.has_pushshift,
        CommentIncomplete.has_reddit,
        User.user_pk,
        CommentIncomplete.body,
        CommentIncomplete.retrieved_on
    )
    full_info = full_info.join(
        Subreddit,
        CommentIncomplete.subreddit_id == "t5_" + Subreddit.rid)
    full_info = full_info.outerjoin(
        Post,
        "t3_" + Post.rid == CommentIncomplete.link_id)
    # this inner join insures that only those are considered whose parents have
    # already been inserted
    full_info = full_info.outerjoin(
        parentComment,
        CommentIncomplete.parent_id == "t1_" + parentComment.rid).filter(
            or_(CommentIncomplete.parent_id.startswith("t3_"),
                parentComment.rid.is_not(None)))
    full_info = full_info.outerjoin(
        User,
        User.username == CommentIncomplete.author)
    # this makes sure only non-inserted are considered
    full_info = full_info.outerjoin(
        Comment, Comment.rid == CommentIncomplete.id).filter(
            Comment.rid.is_(None))
    row = namedtuple("row", ["created_utc", "rid", 'permalink', 'subreddit_pk',
                             'post_pk', 'parent_rid', 'parent_comment_pk',
                             'level', 'has_pushshift', 'has_reddit', 'user_pk',
                             'body', 'retrieved_on'])
    with session_manager.transaction():
        total_number_to_insert = session_manager.query(
            func.count(CommentIncomplete.id)).outerjoin(
                Comment, Comment.rid == CommentIncomplete.id).filter(
                    Comment.rid.is_(None)).scalar()
    counter = 0
    maxlevel = 1000
    start = datetime.now()
    for i in range(1, maxlevel + 1):
        with session_manager.transaction():
            count = full_info.count()
            logging.info(
                f"found {count} independently bulk-insertable comments in "
                f"round {i}.")
            if count == 0:
                break
            for chunk in (list((row(*r) for r in c)) for c in utils.chunkify(
                    full_info, 1000)):
                counter += len(chunk)
                session_manager.add_all(
                    (Comment(
                        created_utc = r.created_utc,
                        rid = r.rid,
                        permalink = r.permalink,
                        subreddit_pk = r.subreddit_pk,
                        post_pk = r.post_pk,
                        parent_rid = r.parent_rid,
                        parent_comment_pk = r.parent_comment_pk,
                        level = r.level,
                        has_pushshift = r.has_pushshift,
                        has_reddit = r.has_reddit,
                        user_pk = r.user_pk,
                        bodies=([CommentBody(
                            retrieved_on=(r.retrieved_on if r.retrieved_on
                                          is not None else 0),
                            text=r.body)] if r.body is
                                not None else [])) for
                     r in chunk))
                session_manager.possibly_commit()
                elapsed = datetime.now() - start
                percentage = counter/total_number_to_insert
                estimated_duration = elapsed/percentage
                estimated_end = (start + estimated_duration).isoformat(sep=' ')
                logging.info(
                    f"inserted and indexed {counter}/{total_number_to_insert} "
                    f"({percentage*100:.2f}%) comments")
                logging.info(
                    f"Estimated end of insertion: {estimated_end}.")

def main():
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,
        handlers=(logging.FileHandler(args.logfile),
                  logging.StreamHandler(sys.stdout)),
        format='%(asctime)s %(levelname)s:%(name)s:%(message)s')

    logging.info(f"started with cli-args '{args}'")
    logger = logging.getLogger('psaw')
    logger.setLevel(logging.WARNING)
    api = psaw.PushshiftAPI()
    session_manager = get_session(args)
    reddit = praw.Reddit(args.praw_config, user_agent="update_subreddit")
    subreddits = [reddit.subreddit(sub_name) for sub_name in args.subreddits]
    # force loading of subreddit
    message_lines = ["found the following subreddits"]
    for subreddit in subreddits:
        message_lines.append(
            f"       Subreddit '{subreddit.display_name}' with id "
            f"'{subreddit.id}'")
    logging.info("\n".join(message_lines))

    # start with pushshift posts
    for subreddit in subreddits:
        newest_post_stamp = get_newest(PushShiftPost, session_manager,
                                       subreddit)
        after = int(round(newest_post_stamp - args.time_overlap * 60))
        post_stream = get_stream(
            api, before=int(datetime.now().timestamp()+30), after=after,
            args=args, ORM_type=PushShiftPost, subreddit=subreddit)

        for chunk in (list(c) for c in
                      utils.chunkify(post_stream, args.chunksize)):
            posts = add_pushshift_post_chunk(chunk, session_manager, args)
            add_incomplete_posts_chunk(posts, session_manager, reddit)

        # add pushshift comments
        newest_comment_stamp = get_newest(PushShiftComment, session_manager,
                                          subreddit)
        after = int(round(newest_comment_stamp - args.time_overlap * 60))
        comment_stream = get_stream(
            api, before=int(datetime.now().timestamp()+30), after=after,
            args=args, ORM_type=PushShiftComment, subreddit=subreddit)

        for chunk in (list(c) for c in
                      utils.chunkify(comment_stream, args.chunksize)):
            comments = add_pushshift_comment_chunk(
                chunk, session_manager, args)
            add_incomplete_comments_chunk(comments, session_manager, reddit)

    fix_orphan_comments(session_manager, reddit, args.chunksize)
    fix_missing_posts(session_manager, reddit, args.chunksize)
    fix_comment_levels(session_manager)
    insert_subreddits(session_manager, reddit)
    insert_flairs(session_manager)
    insert_authors(session_manager)
    insert_posts(session_manager)
    user_cache = User.cache(session_manager)
    subreddit_cache = Subreddit.rid_cache(session_manager)
    # insert_comments(session_manager, user_cache, subreddit_cache)
    insert_comments_by_dependence(session_manager)
    logging.info("leaving")


if __name__ == "__main__":
    main()
