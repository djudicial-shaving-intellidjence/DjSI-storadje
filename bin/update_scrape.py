#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   update_scrape.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   12 Mar 2022

@brief  update an existing scrape of a subreddit

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import psaw
import json

from datetime import datetime, timedelta
import itertools

import logging
import sys

import argparse

from DjSI_storadje import (PushShiftComment, PushShiftPost, initialise_base,
                           crawl_comments, crawl_posts, utils)


def parse_args():
    parser = argparse.ArgumentParser(
        description=("update a scrape of all comments and posts off a "
                     "subreddit using pushshift"))
    parser.add_argument("--db_config", "-d", type=str, default="default",
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("subreddit", type=str,
                        help="name of the subreddit to scrape")
    parser.add_argument("--chunksize", "-n", type=int, default=1000,
                        help=("how many chunks to treat at once, must be "
                              "between 1 and 1000"))
    parser.add_argument("--runtime", '-t', type=float, default=float("inf"),
                        help=("stop scraping after RUNTIME minutes. Useful for "
                              "cron jobs to make sure a new job isn't started "
                              "while the old one is still running."))
    parser.add_argument("logfile", type=str,
                        help="path to a logfile")
    args = parser.parse_args()
    if not (1 <= args.chunksize <=1000):
        parser.error(
            f"Chunksize must be between 1 and 1000, {args.chunksize} isnt.")
    if args.runtime <= 0:
        parser.error("Runtime must be a positive number of minutes")
    return args


def get_session(args):
    return utils.get_session(args.db_config)


def get_oldest_and_newest(ORM_type, session):
    oldest, newest = utils.minmax(
        (json.loads(s[0])["created_utc"] for s
         in session.query(ORM_type.data)))
    return oldest, newest


def get_stream(api, before, after, args, ORM_type):
    funs = {PushShiftPost: crawl_posts,
            PushShiftComment: crawl_comments}
    data_stream = funs[ORM_type](
        api, before=before, after=after,
        subreddit=args.subreddit, chunksize=args.chunksize)
    logging.info("fetched a stream")
    return (ORM_type(rid=d.id, data=json.dumps(d.d_)) for d in data_stream)


def handle_stream(stream, args, session, shutdown_time):
    counter = 0
    for chunk in (
            list(chunk) for chunk in utils.chunkify(stream, args.chunksize)):
        rids = {entry.rid for entry in chunk}
        ORM_type = type(chunk[0])
        already_present = {tup[0] for tup in
                           session.query(ORM_type.rid).filter(
                               ORM_type.rid.in_(rids))}
        session.add_all((entry for entry in chunk
                         if entry.rid not in already_present))
        session.commit()
        counter += len(chunk)
        last_stamp = json.loads(chunk[-1].data)['created_utc']
        date = datetime.fromtimestamp(last_stamp).isoformat(sep=' ')
        message = (f"handled {counter} entries so far, "
                   f"the last one handled was from {date}")
        print(message)
        logging.info(message)
        if datetime.now() > shutdown_time:
            logging.warning(f"Shutting down because runtime limit is reached")


def main():
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,# filename=args.logfile,
        handlers=(logging.FileHandler(args.logfile),
                  logging.StreamHandler(sys.stdout)),
        format='%(asctime)s %(levelname)s:%(name)s:%(message)s')
    logging.info(f"started with cli-args '{args}'")
    session = get_session(args)
    api = psaw.PushshiftAPI()

    shutdown_time = datetime.now() + timedelta(minutes=args.runtime)

    for ORM_type in (PushShiftComment, PushShiftPost):
        oldest, newest = get_oldest_and_newest(ORM_type, session)

        stream_new_entries = get_stream(
            api,
            before=int(datetime.now().timestamp()+1),
            after=int(newest-1),
            args=args, ORM_type=ORM_type)
        handle_stream(stream_new_entries, args, session, shutdown_time)
        stream_old_entries = get_stream(api, before=int(oldest + 1), after=0,
                                        args=args, ORM_type=ORM_type)
        handle_stream(stream_old_entries, args, session, shutdown_time)
    session.commit()


if __name__ == "__main__":
    main()
