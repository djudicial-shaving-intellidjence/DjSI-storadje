#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   scrape_pushshift_subreddit.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   06 Mar 2022

@brief  scrapes all the comments and posts off a subreddit

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import psaw
import json

from datetime import datetime, timedelta
import itertools

import logging

from configparser import ConfigParser
import os
import argparse

from DjSI_storadje import (PushShiftComment, PushShiftPost, initialise_base,
                           crawl_comments, crawl_posts)

def parse_args():
    parser = argparse.ArgumentParser(
        description="scrape all comments and posts off a subreddit using pushshift")
    parser.add_argument("--db_config", "-d", type=str, default="default",
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("subreddit", type=str,
                        help="name of the subreddit to scrape")
    args = parser.parse_args()
    return args


def minmax(it):
    min = max = None
    for val in it:
        if min is None or val < min:
            min = val
        if max is None or val > max:
            max = val
    return min, max


def chunkify(iterable, chunk_size):
    it = iter(iterable)
    while True:
        chunk = itertools.islice(it, chunk_size)
        try:
            first = next(chunk)
        except StopIteration:
            return
        yield itertools.chain((first,), chunk)


def main():
    args = parse_args()

    config_parser = ConfigParser()
    config_parser.read(os.path.expanduser("~/.config/djsia.ini"))

    section = config_parser[args.db_config]
    password = section['password']
    user = section['user']
    host = section['host']
    db = section['db']
    engine = create_engine(
        f"postgresql://{user}:{password}@{host}/{db}", echo=True)
    # engine = create_engine('sqlite:////tmp/testbase.db', echo=True)
    initialise_base(engine)
    session = sessionmaker(bind=engine)()

    api = psaw.PushshiftAPI()

    now = datetime.now()
    duration = timedelta(days=365.25)
    before = int(now.timestamp())
    after = int((now-duration).timestamp())
    chunksize = 1000

    oldest_post, newest_post = minmax(
        (json.loads(s[0])["created_utc"] for s
         in session.query(PushShiftPost.data)))
    oldest_comment, newest_comment = minmax(
        (json.loads(s[0])["created_utc"] for s
         in session.query(PushShiftComment.data)))

    print("comments:")
    print(f"oldest timestamp: {oldest_comment} ({datetime.fromtimestamp(oldest_comment)})")
    print(f"newest timestamp: {newest_comment} ({datetime.fromtimestamp(newest_comment)})")
    print("posts:")
    print(f"oldest timestamp: {oldest_post} ({datetime.fromtimestamp(oldest_post)})")
    print(f"newest timestamp: {newest_post} ({datetime.fromtimestamp(newest_post)})")
    comments = crawl_comments(api, before=before, after=int(newest_comment),
                              subreddit=args.subreddit, chunksize=chunksize,
                              session=session)
    session.add_all(
        (PushShiftComment(rid=comment.id, data=json.dumps(comment.d_))
         for comment in comments
         if session.query(PushShiftComment.rid == comment.id).count() == 0))

    comments = crawl_comments(api, before=oldest_comment, after=0,
                              subreddit=args.subreddit, chunksize=chunksize)

    for chunk in chunkify(
            (PushShiftComment(rid=comment.id, data=json.dumps(comment.d_))
             for comment in comments), chunksize):
        chunklist = list(chunk)
        rids = {v.rid for v in chunklist}
        already_present = {tup[0] for tup in
                           session.query(PushShiftComment.rid).filter(
                               PushShiftComment.rid.in_(rids))}
        session.add_all((v for v in chunklist if v.rid not in already_present))
        session.commit()
        last_stamp = json.loads(chunklist[-1].data)['created_utc']
        date = datetime.fromtimestamp(last_stamp).isoformat(sep=' ')
        message=f"last comment handled was from {date}"
        print(message)
        logging.info(message)

    posts = crawl_posts(api, before=before, after=0,
                        subreddit=args.subreddit, chunksize=chunksize)
    pushshift_posts = (PushShiftPost(rid=post.id,
                                     data=json.dumps(post.d_))
                       for post in posts)

    for chunk in chunkify(pushshift_posts, chunksize):
        chunklist = list(chunk)
        rids = {v.rid for v in chunklist}
        already_present = {tup[0] for tup in session.query(PushShiftPost.rid).filter(
            PushShiftPost.rid.in_(rids))}
        new_posts = [v for v in chunklist if v.rid not in already_present]
        session.add_all(new_posts)
        session.commit()
        last_stamp = json.loads(chunklist[-1].data)['created_utc']
        date = datetime.fromtimestamp(last_stamp).isoformat(sep=' ')
        message=f"last post handled was from {date}"
        print(message)
        logging.info(message)

    # session.add_all(
    #     (PushShiftPost(rid=post.id, data=json.dumps(post.d_))
    #      for post in posts
    #      if session.query(PushShiftPost.rid == post.id).count() == 0))

    # posts = crawl_posts(api, before=oldest_post, after=0,
    #                     subreddit=args.subreddit, chunksize=chunksize,
    #                     session=session)

    # session.add_all(
    #     (PushShiftPost(rid=post.id, data=json.dumps(post.d_))
    #      for post in posts
    #      if session.query(PushShiftPost.rid == post.id).count() == 0))

    session.commit()


if __name__ == "__main__":
    main()
