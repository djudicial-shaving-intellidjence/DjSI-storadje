#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   update_subreddit.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   15 Mar 2022

@brief  updates the database for a subreddit. this is done chronologically to
        minimise overhead

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import psaw
import praw

import logging
import argparse
import sys

from datetime import datetime, timedelta

from DjSI_storadje import (PushShiftComment, PushShiftPost, utils)
from DjSI_storadje import (User, Subreddit, Post, Comment, sanitise_id,
                           is_comment_id, is_post_id)
from sqlalchemy import func


def parse_args():
    parser = argparse.ArgumentParser(
        description=("update a scrape of all comments and posts off a "
                     "subreddit using pushshift and reddit where pushshift "
                     "has gaps. Not suitable for bulk loading!"))
    parser.add_argument("--db_config", "-d", type=str, default="default",
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("praw_config", type=str,
                        help="name of the config session in your praw.ini")
    parser.add_argument("subreddit", type=str,
                        help="name of the subreddit to scrape")
    parser.add_argument("--chunksize", "-n", type=int, default=1000,
                        help=("how many chunks to treat at once, must be "
                              "between 1 and 1000"))
    parser.add_argument("--runtime", '-t', type=float, default=60.,
                        help=("stop scraping after RUNTIME minutes. Useful "
                              "for cron jobs to make sure a new job isn't "
                              "started while the old one is still running."))
    parser.add_argument("--time_overlap", type=float, default=60.,
                        help=("start looking TIME_OVERLAP minutes before, to "
                              "catch newly added older posts or comments"))
    parser.add_argument("logfile", type=str,
                        help="path to a logfile")
    parser.add_argument("--commit", action='store_true',
                        help=("do commit these updates to the database "
                              "(default is just a dry run)"))
    args = parser.parse_args()
    if not (1 <= args.chunksize <= 1000):
        parser.error(
            f"Chunksize must be between 1 and 1000, {args.chunksize} isnt.")
    if args.runtime <= 0:
        parser.error("Runtime must be a positive number of minutes")
    if args.time_overlap <= 0:
        parser.error("overlap time must be a positive number of minutes")
    return args


def get_session(args):
    engine, session = utils.get_session(args.db_config)
    logging.info("fetched the connection engine")
    return engine, session


def get_newest(ORM_type, session, args):
    timestamp = session.query(
        func.max(ORM_type.created_utc)).filter(
            ORM_type.subreddit == args.subreddit).scalar()
    date_str = datetime.fromtimestamp(timestamp).isoformat(
        sep=' ', timespec='seconds')
    logging.info(f"newest entry in {ORM_type.__tablename__} is "
                 f"from {date_str}.")
    return timestamp


def get_stream(api, before, after, args, ORM_type):
    data_stream = ORM_type.crawl(api=api, before=before, after=after,
                                 limit=float("inf"),
                                 subreddit=args.subreddit,
                                 chunksize=args.chunksize,
                                 session=None)
    ORM_stream = (ORM_type.from_json(d.d_) for d in data_stream)
    logging.info(
        f"fetched data stream for {ORM_type.__tablename__}.")
    return ORM_stream


def handle_post_stream(stream, args, session, reddit, shutdown_time,
                       user_cache):
    counter = 0
    for chunk in (list(chunk) for chunk in
                  utils.chunkify(stream, args.chunksize)):
        if datetime.now() > shutdown_time:
            logging.warning(f"Shutting down because runtime limit is reached")
            return
        rids = {entry.rid for entry in chunk}
        already_present = {tup[0] for tup in
                           session.query(Post.rid).filter(
                               Post.rid.in_(rids))}
        to_add = {item for item in chunk if item.rid not in already_present}
        [user_cache[item.data["author"]] for item in to_add if
         item.data["author"] != "[deleted]"]
        session.add_all(to_add)
        session.add_all((Post.from_json(item.data, has_pushshift=True,
                                        has_reddit=False)
                         for item in to_add))
        counter += len(to_add)
        logging.info(f"added {counter} posts so far")


def handle_comment_stream(stream, args, api, session, reddit, shutdown_time,
                          user_cache):
    counter = 0

    def ensure_post_existence(post_rid):
        if session.query(func.count()).filter(
                Post.rid == post_rid).scalar() == 0:
            posts = PushShiftPost.add_by_id(api, (post_rid,), session)
            session.add_all((Post.from_json(p.data, has_pushshift=True,
                                            has_reddit=False) for p in posts))
            if not posts:
                post = reddit.submission(post_rid)
                session.add(Post.from_reddit(post))

    def ensure_comment_existence(comment_id):
        comment_query = session.query(Comment).filter(
                Comment.rid == comment_id)
        if comment_query.count() == 1:
            return comment_query.one().level + 1

        comments = PushShiftComment.add_by_id(api, (comment_id,), session)
        if len(comments) == 1:
            comment = comments[0]
            if comment.data["parent_id"] == comment.data["link_id"]:
                level = 1
            else:
                level = ensure_comment_existence(
                    sanitise_id(comment.data["parent_id"])) + 1

            new_comment = Comment.from_json(comment.data, level=level)
            if new_comment.author is not None:
                user_cache[new_comment.author_name]
            session.add(new_comment)
            session.add(comment)
        else:
            comment = reddit.comment(comment_id)
            if comment.parent_id == comment.link_id:
                level = 1
            else:
                level = ensure_comment_existence(
                    sanitise_id(comment.parent_id)) + 1

            if comment.author is not None:
                user_cache[comment.author.name]
            session.add(Comment.from_reddit(comment, level=level))
        return level

    all_raw_comments = list(stream)
    rids = {c.data["id"] for c in all_raw_comments}
    already_present = {tup[0] for tup in
                       session.query(Comment.rid).filter(
                           Comment.rid.in_(rids))}

    raw_comments = (c for c in all_raw_comments
                    if c.data["id"] not in already_present)
    i = 0
    for i, raw_comment in enumerate(raw_comments, start=1):
        if datetime.now() > shutdown_time:
            logging.warning(f"Shutting down because runtime limit is reached")
            break
        post_rid = sanitise_id(raw_comment.data["link_id"])
        ensure_post_existence(post_rid)

        if is_comment_id(raw_comment.data['parent_id']):
            level = ensure_comment_existence(
                sanitise_id(raw_comment.data['parent_id']))
        else:
            level = 0

        if raw_comment.data['author'] != '[deleted]':
            user_cache[raw_comment.data['author']]
        comment = Comment.from_json(raw_comment.data, level)

        session.add(comment)
        session.add(raw_comment)
        if i % 200 == 0:
            logging.info(f"added {i} comments so far")
    logging.info(f"added a total of {i} comments")


def main():
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,
        handlers=(logging.FileHandler(args.logfile),
                  logging.StreamHandler(sys.stdout)),
        format='%(asctime)s %(levelname)s:%(name)s:%(message)s')
    logging.info(f"started with cli-args '{args}'")
    logger = logging.getLogger('psaw')
    logger.setLevel(logging.WARNING)
    engine, session = get_session(args)
    api = psaw.PushshiftAPI()
    reddit = praw.Reddit(args.praw_config, user_agent="update_subreddit")

    shutdown_time = datetime.now() + timedelta(minutes=args.runtime)
    user_cache = User.inserting_cache(session)

    with session.begin():
        newest_post_stamp = get_newest(PushShiftPost, session, args)
        after = int(round(newest_post_stamp - args.time_overlap * 60))
        post_stream = get_stream(
            api, before=int(datetime.now().timestamp()+30), after=after,
            args=args, ORM_type=PushShiftPost)

        handle_post_stream(post_stream, args, session, reddit, shutdown_time,
                           user_cache)

        newest_comment_stamp = get_newest(PushShiftComment, session, args)
        after = int(round(newest_comment_stamp - args.time_overlap * 60))
        comment_stream = get_stream(
            api, before=int(datetime.now().timestamp()+30), after=after,
            args=args, ORM_type=PushShiftComment)

        try:
            handle_comment_stream(comment_stream, args, api, session, reddit,
                              shutdown_time, user_cache)
        except Exception as err:
            logging.exception("Failed at handling comment stream")
            raise err

        if not args.commit:
            logging.info(
                "rolling everything back because the commit flag  wasn't set")
            session.flush()
            session.rollback()
        else:
            logging.info(
                "committing changes")
    logging.info("leaving")


if __name__ == "__main__":
    main()
