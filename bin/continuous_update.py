#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   continuous_update.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   04 Nov 2022

@brief  refreshing database and keep streaming

Copyright © 2022 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import praw
from prawcore import exceptions
import DjSI_storadje as djs
from DjSI_storadje import PushShiftComment, PushShiftPost
from DjSI_storadje import (User, Subreddit, Post, Comment, sanitise_id,
                           PostFlair, CommentBody, PostBody)

from DjSI_storadje import utils

from datetime import datetime

import argparse
import logging
import sys
from queue import PriorityQueue


class SessionManager():
    class Transaction():
        def __init__(self, session, do_commit):
            self.__session = session
            self.__do_commit = do_commit

        def __enter__(self):
            self.__session.begin()

        def __exit__(self, type, value, traceback):
            if not self.__do_commit:
                self.__session.rollback()
            else:
                self.__session.commit()

    def __init__(self, session, do_commit):
        self.__session = session
        self.__do_commit = do_commit

    def add(self, *args, **kwargs):
        return self.__session.add(*args, **kwargs)

    def add_all(self, *args, **kwargs):
        return self.__session.add_all(*args, **kwargs)

    def query(self, *args, **kwargs):
        return self.__session.query(*args, **kwargs)

    def flush(self):
        return self.__session.flush()

    def execute(self, *args, **kwargs):
        return self.__session.execute(*args, **kwargs)

    def transaction(self):
        return self.Transaction(self.__session, self.__do_commit)

    def possibly_commit(self):
        if self.__do_commit:
            self.__session.commit()


def get_session(args):
    try:
        dummy, session = utils.get_session(args.db_config)
        logging.info("fetched the connection engine")
        return SessionManager(session, args.commit)
    except Exception as err:
        logging.exception("failed to get a db session")
        raise err


def parse_args():
    parser = argparse.ArgumentParser(
        description=(
            "update (or initialise and update) the database for a "
            "subreddit. Update a scrape of all comments and posts off a "
            "subreddit using reddit. Not suitable for bulk loading!"))
    parser.add_argument("db_config", type=str,
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("praw_config", type=str,
                        help="name of the config session in your praw.ini")
    parser.add_argument("--nb_history", type=int, default=10,
                        help=("how many old posts to go look through, between 0 and 1000"))
    parser.add_argument("logfile", type=str,
                        help="path to a logfile")
    parser.add_argument("--commit", action='store_true',
                        help=("do commit these updates to the database "
                              "(default is just a dry run)"))
    parser.add_argument("subreddits", type=str, nargs='+',
                        help="name of the subreddits to scrape")
    args = parser.parse_args()
    if not (0 <= args.nb_history <= 1000):
        parser.error(
            f"nb_history must be between 1 and 1000, {args.chunksize} isnt.")
    return args


class CommentComp(object):
    def __init__(self, comment):
        self.comment = comment

    def __lt__(self, other):
        return self.comment.created_utc < other.comment.created_utc


def add_comment(comment, comments):
    logging.info(f"|Q|={comments.qsize()}: adding {comment.id} by {comment.author}")
    comments.put(CommentComp(comment))


def handle_comment_reply(reddit_comment, parent_id, reddit, queue,
                         session_manager):
    author_pk = ensure_author_existance(reddit_comment.author, session_manager)
    post_pk = ensure_submission_existance(reddit_comment.submission.id, reddit,
                                          session_manager)
    with session_manager.transaction():
        parent_comment = session_manager.query(Comment).filter_by(
            rid=parent_id).one_or_none()
        if parent_comment is None:
            # put this comment back in the queue
            add_comment(reddit_comment, queue)
            # add its parent to the queue. Eventually this will reach an
            # existing comment. Very inefficient, but only a problem during
            # catch-up phase
            add_comment(reddit.comment(parent_id), queue)
            return None
        subreddit = session_manager.query(Subreddit).filter_by(
            rid=reddit_comment.subreddit.id).one()
        # make sure the comment hasn't been introduced by someone else in the
        # meanwhile
        comment = session_manager.query(Comment).filter_by(
            rid=reddit_comment.id).one_or_none()
        if comment is not None:
            return comment.comment_pk
        comment = Comment(created_utc=reddit_comment.created_utc,
                          rid=reddit_comment.id,
                          permalink=reddit_comment.permalink,
                          subreddit_pk=subreddit.subreddit_pk,
                          post_pk=post_pk,
                          parent_rid=parent_id,
                          level=parent_comment.level+1,
                          has_pushshift=False,
                          has_reddit=True,
                          user_pk=author_pk,
                          bodies=[CommentBody(
                              retrieved_on=datetime.now().timestamp(),
                              text=reddit_comment.body)])
        session_manager.add(comment)
        session_manager.flush()
        return comment.comment_pk


def handle_top_level_comment(reddit_comment, submission_id, reddit, queue,
                             session_manager):
    submission = reddit.submission(submission_id)
    post_pk = ensure_submission_existance(submission_id, reddit,
                                          session_manager)
    author_pk = ensure_author_existance(reddit_comment.author, session_manager)
    with session_manager.transaction():
        subreddit = session_manager.query(Subreddit).filter_by(
            rid=submission.subreddit.id).one()
        # make sure the comment hasn't been introduced by someone else in the
        # meanwhile
        comment = session_manager.query(Comment).filter_by(
            rid=reddit_comment.id).one_or_none()
        if comment is not None:
            return comment.comment_pk

        comment = Comment(created_utc=reddit_comment.created_utc,
                          rid=reddit_comment.id,
                          permalink=reddit_comment.permalink,
                          subreddit_pk=subreddit.subreddit_pk,
                          post_pk=post_pk,
                          parent_rid=submission_id,
                          level=0,
                          has_pushshift=False,
                          has_reddit=True,
                          user_pk=author_pk,
                          bodies=[CommentBody(
                              retrieved_on=datetime.now().timestamp(),
                              text=reddit_comment.body)])
        session_manager.add(comment)
        session_manager.flush()
        return comment.comment_pk


def ensure_author_existance(reddit_user, session_manager):
    if reddit_user is None:
        return None
    with session_manager.transaction():
        author = session_manager.query(User).filter_by(
            username=reddit_user.name).one_or_none()
        if author:
            return author.user_pk
        author = User(username=reddit_user.name)
        session_manager.add(author)
        session_manager.flush()
        return author.user_pk


def ensure_flair_existance(flair, session_manager):
    with session_manager.transaction():
        author = session_manager.query(PostFlair).filter_by(
            label=flair).one_or_none()
        if author:
            return author.postflair_pk
        author = PostFlair(label=flair)
        session_manager.add(author)
        session_manager.flush()
        return author.postflair_pk


def ensure_submission_existance(submission_id, reddit, session_manager):
    with session_manager.transaction():
        post = session_manager.query(Post).filter_by(
            rid=submission_id).one_or_none()
        if post is not None:
            return post.post_pk

    submission = reddit.submission(submission_id)
    flair_pk = None
    if submission.link_flair_text:
        flair_pk = ensure_flair_existance(submission.link_flair_text,
                                          session_manager)
    author_pk = ensure_author_existance(submission.author, session_manager)

    with session_manager.transaction():
        # yes, read the post again to make sure it hasn't been inserted by some
        # other process in the meanwhile
        post = session_manager.query(Post).filter_by(
            rid=submission_id).one_or_none()
        if post is not None:
            return post.post_pk
        subreddit = session_manager.query(Subreddit).filter_by(
            rid=submission.subreddit.id).one()
        assert submission_id == submission.id

        post = Post(created_utc=submission.created_utc,
                    rid=submission_id,
                    permalink=submission.permalink,
                    title=submission.title,
                    subreddit_pk=subreddit.subreddit_pk,
                    has_pushshift=False,
                    has_reddit=True,
                    user_pk=author_pk,
                    postflair_pk=flair_pk,
                    bodies=[PostBody(
                        retrieved_on=datetime.now().timestamp(),
                        title=submission.title,
                        text=submission.selftext)])
        session_manager.add(post)
        session_manager.flush()
        post_pk = post.post_pk
    return post_pk


def insert_comment(reddit_comment, reddit,  queue, session_manager):

    ensure_author_existance(reddit_comment.author, session_manager)

    parent_id = reddit_comment.parent_id
    if parent_id.startswith("t1_"):
        # it's a reply to a comment
        handle_comment_reply(reddit_comment, parent_id.lstrip("t1_"), reddit,
                             queue, session_manager)

    if parent_id.startswith("t3_"):
        # it's a top level comment
        handle_top_level_comment(reddit_comment, parent_id.lstrip("t3_"),
                                 reddit, queue, session_manager)


def main():
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,
        handlers=(logging.FileHandler(args.logfile),
                  logging.StreamHandler(sys.stdout)),
        format='%(asctime)s %(levelname)s:%(name)s:%(message)s')

    logging.info(f"started with cli-args '{args}'")
    logger = logging.getLogger('psaw')
    logger.setLevel(logging.WARNING)
    try:

        session_manager = get_session(args)
        reddit = praw.Reddit(args.praw_config, user_agent="update_subreddit")
        subreddits = [reddit.subreddit(sub_name) for sub_name in args.subreddits]
        # force loading of subreddit
        message_lines = ["found the following subreddits"]
        for subreddit in subreddits:
            message_lines.append(
                f"       Subreddit '{subreddit.display_name}' with id "
                f"'{subreddit.id}'")
        logging.info("\n".join(message_lines))

        comments = PriorityQueue()
        streams = [subreddit.stream for subreddit in subreddits]

        running = True

        def comment_stream():
            comment_streams = [stream.comments(pause_after=0)
                               for stream in streams]
            while running:
                for stream in comment_streams:
                    for comment in stream:
                        if comment is None:
                            break
                        yield comment
                yield None

        central_stream = comment_stream()

        # start reading the streams
        for comment in central_stream:
            if comment is None:
                break

            add_comment(comment, comments)

        # read old submissions
        for subreddit in subreddits:
            for nb, submission in enumerate(subreddit.new(limit=args.nb_history)):
                logging.info(f"dealing with submission {nb}/{args.nb_history} for "
                             f"subreddit '{subreddit.display_name}'")
                submission.comments.replace_more(limit=None)
                for comment in submission.comments.list():
                    add_comment(comment, comments)

        while True:
            next_comment = next(central_stream)

            if next_comment is not None:
                add_comment(next_comment, comments)
            while not comments.empty():
                reddit_comment = comments.get().comment
                with session_manager.transaction():
                    dj_comment = session_manager.query(Comment).filter_by(
                        rid=reddit_comment.id).one_or_none()
                    print(dj_comment, f"queue_size = {comments.qsize()}")
                    if dj_comment is not None:
                        # Comment already in data base
                        print(
                            f"comment {dj_comment} by {dj_comment.author} already in db")
                        comments.task_done()
                        continue
                    print(
                        f"New comment {reddit_comment} by {reddit_comment.author}!"
                        f"\n{reddit_comment.body}\n########################")

                insert_comment(reddit_comment, reddit, comments,
                               session_manager)

                comments.task_done()
    except exceptions.NotFound as err:
        logging.info("Caught Reddit error {err}, starting over")
        main()


if __name__ == "__main__":
    main()
