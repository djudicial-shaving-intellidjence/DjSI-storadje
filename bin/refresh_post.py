#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   refresh_post.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Aug 2022

@brief  refresh all the comments in a given post 

Copyright © 2022 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import praw
import DjSI_storadje as Djs

import logging
import sys

from .quick_build import get_session
import argparse


def parse_args():

    parser = argparse.ArgumentParser(
        description=("check whether any comments in a post received an update "
                     "and refresh the comment bodies if yes"))
    parser.add_argument("db_config", type=str,
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("praw_config", type=str,
                        help="name of the config session in your praw.ini")
    parser.add_argument("--chunksize", "-n", type=int, default=1000,
                        help=("how many chunks to treat at once, must be "
                              "between 1 and 1000"))

    parser.add_argument("logfile", type=str,
                        help="path to a logfile")
    parser.add_argument("--commit", action='store_true',
                        help=("do commit these updates to the database "
                              "(default is just a dry run)"))
    parser.add_argument("post_rid", type=str,
                        help="reddit id of the post to refresh")

    args = parser.parse_args()
    if not (1 <= args.chunksize <= 1000):
        parser.error(
            f"Chunksize must be between 1 and 1000, {args.chunksize} isnt.")

    return args


def main():
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,
        handlers=(logging.FileHandler(args.logfile),
                  logging.StreamHandler(sys.stdout)),
        format='%(asctime)s %(levelname)s:%(name)s:%(message)s')

    logging.info(f"started with cli-args '{args}'")
    logger = logging.getLogger('psaw')
    logger.setLevel(logging.WARNING)

    reddit = praw.Reddit(args.praw_config, user_agent="refresh post")

    session_manager = get_session(args)


    with session_manager.transaction():
        post = session_manager.query(Djs.Post).filter_by(
            rid=args.post_rid).one_or_none()
        if post is None:
            logging.exception(f"failed to fetch post '{args.post_rid}'")
            return 1
        nb_refreshs = post.refresh(reddit)
        logging.info(f"refreshed {nb_refreshs} comments")
        print(session_manager.dirty)
        return 0


if __name__ == "__main__":
    main()
