#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   find_AA_post.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   03 Aug 2022

@brief  find the Austere August post for a given day and return its id

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import DjSI_storadje as Djs

import logging
import sys

from datetime import datetime, timedelta
import pytz

from .quick_build import get_session
import argparse

timezone = pytz.timezone("US/Pacific")

def parse_args():

    parser = argparse.ArgumentParser(
        description=("Find the reddit id for a Austere August post"))
    parser.add_argument("db_config", type=str,
                        help="name of the config section in djsia.ini to use")
    parser.add_argument("logfile", type=str,
                        help="path to a logfile")
    parser.add_argument("day", type=int,
                        help="deadline before considered late")

    args = parser.parse_args()
    args.commit=False
    if not (1 <= args.day <= 31):
        parser.error(
            f"day must be between 1 and 31.")

    return args


def main():
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,
        handlers=(logging.FileHandler(args.logfile),
                  logging.StreamHandler(sys.stdout)),
        format='%(asctime)s %(levelname)s:%(name)s:%(message)s')

    logging.info(f"started with cli-args '{args}'")
    logger = logging.getLogger('psaw')
    logger.setLevel(logging.WARNING)
    session_manager = get_session(args)
    return work(session_manager, args.day)

def get_title(day):
    year = datetime.now().year
    date = datetime(year, 8, day)
    title = date.strftime(
        "%A Austere August SOTD Thread - %b %d, %Y")
    return title, date

def work(session_manager, day):
    with session_manager.transaction():
        title, date = get_title(day)
        cutoff = timezone.localize(date +
                                   timedelta(days=1))
        post  = session_manager.query(Djs.Post).filter_by(
            title=title).one()
        logging.info(f"Found id '{post.rid}' for post '{title}'")
        return post.rid
