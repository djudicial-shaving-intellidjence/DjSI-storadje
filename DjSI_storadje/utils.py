#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   utils.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   12 Mar 2022

@brief  small utility functions

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import itertools
from configparser import ConfigParser
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .base import initialise_base


def minmax(it):
    min = max = None
    for val in it:
        if min is None or val < min:
            min = val
        if max is None or val > max:
            max = val
    return min, max


def chunkify(iterable, chunk_size):
    it = iter(iterable)
    while True:
        try:
            chunk = itertools.islice(it, chunk_size)
        except ValueError as err:
            raise ValueError(
                "caught error {err} while trying to chunkify with chunk_size "
                f"= {chunk_size}")
        try:
            first = next(chunk)
        except StopIteration:
            return
        yield itertools.chain((first,), chunk)


def get_session(conf_name='default', echo=False):
    config_parser = ConfigParser()
    config_parser.read(os.path.expanduser("~/.config/djsia.ini"))

    section = config_parser[conf_name]
    password = section['password']
    user = section['user']
    host = section['host']
    db = section['db']
    engine = create_engine(
        f"postgresql://{user}:{password}@{host}/{db}",
        echo=echo)
    initialise_base(engine)
    session = sessionmaker(bind=engine)()

    return engine, session
