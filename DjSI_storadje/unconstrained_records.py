#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   unconstrained_records.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   28 Mar 2022

@brief  sligthly prepped pushshift records

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .base import StoradjeBase

from sqlalchemy import (Column, Integer, Boolean, String, ForeignKey,
                        UnicodeText, TypeDecorator, Computed, Index,
                        UniqueConstraint, func, Text)

import json


class CommentIncomplete(StoradjeBase):
    __tablename__ = "comments_incomplete"
    id = Column(String, primary_key=True)
    author = Column(String)
    created_utc = Column(Integer, nullable=False)
    permalink = Column(String)
    body = Column(UnicodeText)
    subreddit_id = Column(Text)
    link_id = Column(Text)
    parent_id = Column(Text)
    retrieved_on = Column(Integer)
    level = Column(Integer)
    has_pushshift = Column(Boolean, nullable=False)
    has_reddit = Column(Boolean, nullable=False)


class PostIncomplete(StoradjeBase):
    __tablename__ = "posts_incomplete"
    id = Column(String, primary_key=True)
    author = Column(String)
    created_utc = Column(Integer, nullable=False)
    permalink = Column(String)
    title = Column(String)
    body = Column(UnicodeText)
    subreddit_id = Column(Text)
    retrieved_on = Column(Integer)
    link_flair_text = Column(Text)
    has_pushshift = Column(Boolean, nullable=False)
    has_reddit = Column(Boolean, nullable=False)
