#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   reddit_persistence.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   13 Mar 2022

@brief  persistent storage for reddit objects. These are meant to be used in
        analyses

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sqlalchemy import (Column, Integer, Boolean, String, ForeignKey,
                        UnicodeText, TypeDecorator, Computed, Index,
                        UniqueConstraint, func, Text)
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects import postgresql


from datetime import datetime

from .base import StoradjeBase
from .utils import chunkify

import re

__id_pattern = re.compile("(t[0-9]_)?(.*$)")


def split_id(rid):
    match = __id_pattern.match(rid)
    if not match:
        raise Exception(
            f"Couldn't understand the pattern of reddit id '{rid}'.")
    return (match.group(1), match.group(2))


def sanitise_id(rid):
    return split_id(rid)[1]


def is_comment_id(rid):
    return split_id(rid)[0].startswith("t1_")


def is_post_id(rid):
    return split_id(rid)[0].startswith("t3_")


class DefaultDictWithKeyedFactory(dict):
    def __init__(self, factory):
        self.factory = factory

    def __missing__(self, key):
        self[key] = self.factory(key)
        return self[key]


class User(StoradjeBase):
    __tablename__ = "users"
    user_pk = Column(Integer, primary_key=True)
    username = Column(String, nullable=False, unique=True)
    username_lower = Column(String, Computed("lower(username)", persisted=True),
                            nullable=False, unique=True)
    posts = relationship("Post", order_by="Post.created_utc",
                         back_populates='author')
    comments = relationship("Comment", order_by="Comment.created_utc",
                            back_populates='author')

    def __repr__(self):
        return f"<User('{self.username}')>"

    @classmethod
    def exist(cls, username, session):
        return session.query(cls).filter(cls.username == username).count() == 1

    @classmethod
    def fetch(cls, username, session):
        try:
            return session.query(cls).filter(cls.username == username).one()
        except NoResultFound as err:
            raise NoResultFound(
                f"Couldn't find a user with username == {username}")

    @classmethod
    def cache(cls, session):
        return DefaultDictWithKeyedFactory(lambda key: cls.fetch(key, session))

    @classmethod
    def inserting_cache(cls, session):
        def factory(username):
            try:
                return cls.fetch(username, session)
            except NoResultFound:
                user = cls(username=username)
                session.add(user)
                return user
        return DefaultDictWithKeyedFactory(factory)


class Subreddit(StoradjeBase):
    __tablename__ = "subreddits"
    subreddit_pk = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    name_lower = Column(String, Computed("lower(name)", persisted=True),
                        nullable=False, unique=True)
    rid = Column(String, nullable=False, unique=True)
    posts = relationship("Post", order_by="Post.created_utc",
                         back_populates='subreddit')
    comments = relationship("Comment", order_by="Comment.created_utc",
                            back_populates='subreddit')

    def __repr__(self):
        return (f"<Subreddit('{self.name}')>")

    @classmethod
    def exists(cls, name, session):
        return session.query(cls).filter(
            cls.name_lower == name.lower()).count() == 1

    @classmethod
    def fetch(cls, name, session):
        return session.query(cls).filter(cls.name_lower == name.lower()).one()

    @classmethod
    def cache(cls, session):
        return DefaultDictWithKeyedFactory(lambda key: cls.fetch(key, session))

    @classmethod
    def rid_cache(cls, session):
        def factory(key):
            try:
                return session.query(cls).filter(cls.rid == key).one()
            except NoResultFound as err:
                raise NoResultFound(f"Couldn't find a subreddit with rid == {key}")
        return DefaultDictWithKeyedFactory(factory)


class TSVector(TypeDecorator):
    impl = postgresql.TSVECTOR
    cache_ok = True


class PostBody(StoradjeBase):
    __tablename__ = "postbodies"
    post_body_pk = Column(Integer, primary_key=True)
    post_pk = Column(Integer, ForeignKey('posts.post_pk'), nullable=False)
    post = relationship("Post", back_populates='bodies')
    retrieved_on = Column(Integer, nullable=False)
    text = Column(UnicodeText, nullable=False)
    title = Column(UnicodeText, nullable=False)  # copy, for  generated column
    text_hash = Column(Text, Computed("MD5(text)", persisted=True))
    ts_vector_text = Column(
        TSVector,
        Computed(
            "to_tsvector('english', "
            " title || ' ' || text)",
            persisted=True))
    __table_args__ = (Index('ix_ts_vector_post_text', ts_vector_text,
                            postgresql_using="gin"),
                      UniqueConstraint("post_pk", "text_hash", "retrieved_on",
                                       name='_unique_post_body'))


class PostFlair(StoradjeBase):
    __tablename__ = 'postflairs'
    postflair_pk = Column(Integer, primary_key=True)
    label = Column(String, nullable=False, unique=True)
    posts = relationship("Post", back_populates="flair",
                         order_by="Post.created_utc")

    def __repr__(self):
        return f"<Flair('{self.label}')>"

    @classmethod
    def fetch(cls, label, session):
        try:
            return session.query(cls).filter(cls.label == label).one()
        except NoResultFound:
            raise NoResultFound(f"Couldn't find the label '{label}'")

    @classmethod
    def cache(cls, session):
        def factory(label):
            return cls.fetch(label, session)
        return DefaultDictWithKeyedFactory(factory)

    @classmethod
    def inserting_cache(cls, session):
        def factory(label):
            try:
                return cls.fetch(label, session)
            except NoResultFound:
                flair = cls(label=label)
                session.add(flair)
                return flair
        return DefaultDictWithKeyedFactory(factory)


class Post(StoradjeBase):
    __tablename__ = 'posts'
    post_pk = Column(Integer, primary_key=True)
    user_pk = Column(Integer, ForeignKey("users.user_pk"))
    author = relationship("User", back_populates="posts")
    created_utc = Column(Integer, nullable=False)
    rid = Column(String, nullable=False, unique=True)
    permalink = Column(String, nullable=False, unique=True)
    bodies = relationship("PostBody", back_populates='post')
    subreddit_pk = Column(Integer, ForeignKey("subreddits.subreddit_pk"),
                          nullable=False)
    subreddit = relationship("Subreddit", back_populates="posts")
    title = Column(UnicodeText, nullable=False)
    comments = relationship("Comment", order_by="Comment.created_utc",
                            back_populates="post")
    postflair_pk = Column(Integer, ForeignKey("postflairs.postflair_pk"))
    flair = relationship("PostFlair", back_populates='posts')
    has_pushshift = Column(Boolean, nullable=False)
    has_reddit = Column(Boolean, nullable=False)

    def refresh(self, reddit_instance, chunk_size = 1000):
        print(f"found {len(self.comments)} comments")
        counter = 0
        for comments in chunkify(self.comments, chunk_size):
            comments = list(comments)
            comment_dict = {comment.rid: comment for comment in comments}
            reddit_comments = reddit_instance.info((f"t1_{comment.rid}" for comment in comments))
            for reddit_comment in reddit_comments:
                counter += comment_dict[reddit_comment.id].refresh(
                    reddit_comment)
        return counter
        

    @property
    def reddit_link(self):
        return f"https://reddit.com{self.permalink}"

    @property
    def creation_date(self):
        return datetime.fromtimestamp(self.created_utc)

    def __repr__(self):
        return (f"<Post('{self.title}', created "
                f"{self.creation_date.isoformat(' ')})>")

    @property
    def body(self):
        return self.bodies[-1].text

    @classmethod
    def from_json(cls, json, session, has_pushshift, has_reddit):
        try:
            sub_pk = session.query(Subreddit.subreddit_pk).filter(
                Subreddit.rid == sanitise_id(
                    json["subreddit_id"])).scalar()
        except Exception as err:
            print(err)
            raise err
        kwargs = dict(created_utc=json['created_utc'],
                      rid=json["id"],
                      permalink=json["permalink"],
                      title=json["title"],
                      subreddit_pk=sub_pk,
                      has_pushshift=has_pushshift,
                      has_reddit=has_reddit)
        if json["author"] != "[deleted]":
            kwargs["user_pk"] = session.query(
                User.user_pk).filter(User.username == json["author"]).scalar(),
        if "link_flair_text" in json.keys():
            kwargs["postflair_pk"] = session.query(
                PostFlair.postflair_pk).filter(
                    PostFlair.label ==
                    json["link_flair_text"]).scalar()
        if "selftext" in json.keys():
            kwargs['bodies'] = [PostBody(retrieved_on=json["retrieved_on"],
                                         title=json["title"],
                                         text=json["selftext"])]
        post = cls(**kwargs)
        return post

    @classmethod
    def from_incomplete_post(cls, ipost, session):
        try:
            sub_pk = session.query(Subreddit.subreddit_pk).filter(
                Subreddit.rid == sanitise_id(
                    ipost.subreddit_id)).scalar()
        except Exception as err:
            print(err)
            raise err
        kwargs = dict(created_utc=ipost.created_utc,
                      rid=ipost.id,
                      permalink=ipost.permalink,
                      title=ipost.title,
                      subreddit_pk=sub_pk,
                      has_pushshift=ipost.has_pushshift,
                      has_reddit=ipost.has_reddit)
        if ipost.author != "[deleted]":
            kwargs["user_pk"] = session.query(
                User.user_pk).filter(User.username == ipost.author).scalar(),
        if ipost.link_flair_text is not None:
            kwargs["postflair_pk"] = session.query(
                PostFlair.postflair_pk).filter(
                    PostFlair.label ==
                    ipost.link_flair_text).scalar()
        if ipost.body is not None:
            kwargs["bodies"] = [PostBody(retrieved_on=ipost.retrieved_on,
                                         title=ipost.title,
                                         text=ipost.body)]
        post = cls(**kwargs)
        return post

    @classmethod
    def from_reddit(cls, reddit_submission, session, has_pushshift=False):
        kwargs = dict(created_utc=reddit_submission.created_utc,
                      rid=sanitise_id(reddit_submission.id),
                      permalink=reddit_submission.permalink,
                      title=reddit_submission.title,
                      subreddit_pk=session.query(Subreddit.subreddit_pk).filter(
                          Subreddit.rid == sanitise_id(
                              sanitise_id(
                                  reddit_submission.subreddit_id))).scalar(),
                      has_pushshift=has_pushshift,
                      has_reddit=True)
        if reddit_submission.author is not None:
            kwargs["user_pk"] = session.query(
                User.user_pk).filter(
                    User.username == reddit_submission.author.name).scalar()
        try:
            kwargs["postflair_pk"] = session.query(
                PostFlair.postflair_pk).filter(
                    PostFlair.label ==
                    reddit_submission.link_flair_text).scalar()
        except AttributeError:
            pass
        try:
            kwargs["bodies"] = [
                PostBody(retrieved_on=int(datetime.now.timestamp()),
                         title=reddit_submission.title,
                         text=reddit_submission.selftext)]
        except AttributeError:
            pass
        post = cls(**kwargs)
        return post


class CommentBody(StoradjeBase):
    __tablename__ = "commentbodies"
    commentbody_pk = Column(Integer, primary_key=True)
    comment_pk = Column(Integer, ForeignKey('comments.comment_pk'),
                        nullable=False)
    comment = relationship("Comment", back_populates='bodies')
    retrieved_on = Column(Integer, nullable=False)
    text = Column(UnicodeText, nullable=False)
    text_hash = Column(Text, Computed("MD5(text)", persisted=True))
    ts_vector_text = Column(TSVector, Computed("to_tsvector('english', text)",
                                               persisted=True))
    __table_args__ = (Index('ix_ts_vector_comment_text', ts_vector_text,
                            postgresql_using="gin"),
                      UniqueConstraint("comment_pk", "text_hash", "retrieved_on",
                                       name='_unique_comment_body'))


class Comment(StoradjeBase):
    __tablename__ = "comments"
    comment_pk = Column(Integer, primary_key=True)
    user_pk = Column(Integer, ForeignKey("users.user_pk"))
    author = relationship("User", back_populates="comments")
    created_utc = Column(Integer, nullable=False)
    rid = Column(String, nullable=False, unique=True)
    permalink = Column(String, nullable=False, unique=True)
    bodies = relationship("CommentBody", order_by="CommentBody.retrieved_on",
                          back_populates='comment')
    subreddit_pk = Column(Integer, ForeignKey("subreddits.subreddit_pk"),
                          nullable=False)
    subreddit = relationship("Subreddit", back_populates="comments")
    post_pk = Column(Integer, ForeignKey('posts.post_pk'), nullable=False)
    post = relationship(Post, back_populates="comments")
    parent_rid = Column(String, nullable=False)
    parent_comment_pk = Column(Integer, ForeignKey("comments.comment_pk"))
    replies = relationship("Comment",
                           backref=backref("parent_comment",
                                           remote_side=comment_pk),
                           order_by="Comment.created_utc")
    level = Column(Integer, nullable=False)
    has_pushshift = Column(Boolean, nullable=False)
    has_reddit = Column(Boolean, nullable=False)

    def refresh(self, reddit_comment):
        if reddit_comment.body is not None and self.body != reddit_comment.body:
            retrieved_on = int(datetime.now().timestamp())
            self.bodies.append(CommentBody(retrieved_on=retrieved_on,
                                           text=reddit_comment.body))
            return 1
        return 0

    def re_estimate_level(self):
        if self.parent_comment is None:
            return 0
        return self.parent_comment.re_estimate_level() + 1

    @property
    def genealogy(self):
        """
        return a tuple of all parents up to myself
        """

        if self.parent_comment is None:
            return (self,)
        else:
            return self.parent_comment.genealogy + (self,)

    @property
    def all_parent_comments(self):
        if self.parent_comment is None:
            return list()
        else:
            return self.parent_comment.genealogy

    @property
    def body(self):
        return self.bodies[-1].text

    @property
    def creation_date(self):
        return datetime.fromtimestamp(self.created_utc)

    @property
    def reddit_link(self):
        return f"https://reddit.com{self.permalink}?context={self.level}"

    def __repr__(self):
        author = (self.author.username
                  if self.author is not None else '[deleted]')
        return (f"<Comment(author='{author}', created "
                f"{self.creation_date.isoformat(' ')})>")

    @classmethod
    def from_json(cls, json, level, session):
        kwargs = dict(
            created_utc=json['created_utc'],
            rid=json["id"],
            permalink=json["permalink"],
            subreddit_pk=session.query(Subreddit.subreddit_pk).filter(
                          Subreddit.rid == sanitise_id(
                              sanitise_id(json["subreddit_id"]))).scalar(),
            post_pk=session.query(Post.post_pk).filter(
                Post.rid == sanitise_id(json["link_id"])).scalar(),
            parent_rid=json['parent_id'],
            parent_comment_pk=(session.query(Comment.comment_pk).filter(
                Comment.rid == sanitise_id(json['parent_id'])).scalar() if
                               is_comment_id(json['parent_id'])
                               else None),
            level=level,
            has_pushshift=True,
            has_reddit=False)
        if level == 0 and json['parent_id'] != json['link_id']:
            raise AttributeError(
                f"Cannot have level = 0 if the parent_id ({json['parent_id']}) "
                f"is different from the post_id ({json['link_id']})")
        if json["author"] != "[deleted]":
            kwargs["user_pk"] = session.query(User.user_pk).filter(
                User.username == json["author"]).scalar()

        comment = cls(**kwargs)
        if "retrieved_on" not in json.keys():
            json["retrieved_on"] = int(datetime.now().timestamp())
        comment.bodies.append(CommentBody(retrieved_on=json["retrieved_on"],
                                          text=json["body"]))
        return comment

    @classmethod
    def from_incomplete_comment(cls, icomment, session, user_cache,
                                subreddit_cache, parent_comment_pk=-1):
        if parent_comment_pk == -1:
            parent_comment_pk = (session.query(Comment.comment_pk).filter(
                Comment.rid == sanitise_id(icomment.parent_id)).scalar() if
                                 is_comment_id(icomment.parent_id)
                                 else None)
        kwargs = dict(
            created_utc=icomment.created_utc,
            rid=icomment.id,
            permalink=icomment.permalink,
            subreddit_pk=subreddit_cache[sanitise_id(
                icomment.subreddit_id)].subreddit_pk,
            post_pk=session.query(Post.post_pk).filter(
                Post.rid == sanitise_id(icomment.link_id)).scalar(),
            parent_rid=icomment.parent_id,
            parent_comment_pk=parent_comment_pk,
            level=icomment.level,
            has_pushshift=icomment.has_pushshift,
            has_reddit=icomment.has_reddit)
        if icomment.level == 0 and icomment.parent_id != icomment.link_id:
            raise AttributeError(
                f"Cannot have level = 0 if the parent_id ({icomment['parent_id']}) "
                f"is different from the post_id ({icomment['link_id']})")
        if icomment.author is not None:
            kwargs["user_pk"] = user_cache[icomment.author].user_pk

        comment = cls(**kwargs)
        retrieved_on = (icomment.retrieved_on
                        if icomment.retrieved_on is not None
                        else 0)
        comment.bodies.append(CommentBody(retrieved_on=retrieved_on,
                                          text=icomment.body))
        return comment


    @classmethod
    def from_reddit(cls, reddit_comment, level, session):
        kwargs = dict(
            created_utc=reddit_comment.created_utc,
            rid=reddit_comment.id,
            permalink=reddit_comment.permalink,
            subreddit_pk=session.query(Subreddit.subreddit_pk).filter(
                          Subreddit.rid == sanitise_id(
                              sanitise_id(
                                  reddit_comment.subreddit_id))).scalar(),
            post_pk=session.query(Post.post_pk).filter(
                Post.rid == sanitise_id(reddit_comment.link_id)).scalar(),
            parent_rid=reddit_comment.parent_id,
            parent_comment_pk=(session.query(Comment.comment_pk).filter(
                Comment.rid == sanitise_id(
                    reddit_comment.parent_id)).scalar() if
                               is_comment_id(reddit_comment.parent_id)
                               else None),
            has_pushshift=False,
            has_reddit=True,
            level=level)
        if level == 0 and reddit_comment.parent_id != reddit_comment.link_id:
            raise AttributeError(
                f"Cannot have level = 0 if the parent_id "
                f"({reddit_comment.parent_id}) "
                f"is different from the post_id ({reddit_comment.link_id})")
        if reddit_comment.author is not None:
            kwargs["user_pk"] = session.query(User.user_pk).filter(
                User.username == reddit_comment.author.name).scalar()
        comment = cls(**kwargs)
        comment.bodies.append(CommentBody(
            retrieved_on=int(datetime.now().timestamp()),
            text=reddit_comment.body))
        return comment
