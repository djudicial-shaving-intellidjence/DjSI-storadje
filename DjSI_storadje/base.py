#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   base.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   06 Mar 2022

@brief  base class for all data base mappings

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sqlalchemy.orm import declarative_base as __declarative_base

StoradjeBase = __declarative_base()


def initialise_base(engine):
    StoradjeBase.metadata.create_all(engine)
