#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   __init__.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   05 Mar 2022

@brief  DjSI_storadje is a library to cache, combine, and analyse reddit and
        pushshift data. It is part of the Djudicial Shaving Intellidjence
        Adjency

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from .raw_pushshift_records import (PushShiftComment, PushShiftPost,
                                    crawl_comments, crawl_posts,
                                    add_post_by_id, add_comment_by_id,
                                    crawlers)

from .base import StoradjeBase, initialise_base

from .reddit_persistence import (User, Subreddit, Post, Comment,
                                 PostFlair, sanitise_id, split_id,
                                 is_comment_id, is_post_id,
                                 CommentBody, PostBody)

from .unconstrained_records import CommentIncomplete, PostIncomplete

from . import utils
