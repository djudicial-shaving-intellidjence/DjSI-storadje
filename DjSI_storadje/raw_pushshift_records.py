#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   raw_pushshift_records.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   06 Mar 2022

@brief  persistent storage for pushshift records

Copyright © 2022 Djundjila

DjSI_storadje is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjSI_storadje is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .base import StoradjeBase

from sqlalchemy import Column, Integer, String, JSON

import json


def crawl(api, searchname, subreddit, limit, before, after, chunksize,
          session):
    counter = 0
    this_after = after
    while counter < limit:
        this_chunk = min(chunksize, limit-counter)
        after = max(this_after, after)
        items = getattr(api, searchname)(limit=this_chunk, before=before,
                                         after=after, subreddit=subreddit,
                                         sort="asc")
        this_counter = 0
        for item in items:
            this_counter += 1
            try:
                this_after = item.created_utc
                yield item
            except Exception as err:
                print(
                    f"ecounterened error '{err}' while handling item '{item}' "
                    f"with id '{item.id}'")
            pass
        counter += this_counter
        if session:
            session.commit()
        if this_counter == 0:
            return


def crawl_comments(api, subreddit, limit=float("inf"),
                   before=None, after=None, chunksize=1000, session=None,
                   **kwargs):
    return crawl(api, 'search_comments', subreddit, limit=limit, before=before,
                 after=after, chunksize=chunksize, session=session, **kwargs)


def crawl_posts(api, subreddit, limit=float("inf"),
                before=None, after=None, chunksize=1000, session=None, **kwargs):
    return crawl(api, 'search_submissions', subreddit, limit=limit, before=before,
                 after=after, chunksize=chunksize, session=session, **kwargs)


def _add_by_id(cls, api, ids, session):
    values = getattr(api, cls.searchname)(ids=ids, metadata=True, limit=1)
    items = [cls(rid=v.id, data=json.dumps(v.d_), created_utc=v.d_['created_utc'],
                 subreddit=v.d_['subreddit'])
             for v in values]
    session.add_all(items)
    return list(session.query(cls).filter(cls.rid.in_(ids)))


def add_comment_by_id(api, ids, session):
    return add_by_id(api, ids, session, PushShiftComment)


def add_post_by_id(api, ids, session):
    return add_by_id(api, ids, session, PushShiftPost)


def _classmethod_crawl(cls, api, subreddit, limit, before, after, chunksize,
                       session):
    return crawl(
        api, cls.searchname, subreddit, limit, before, after, chunksize, session)


def _classmethod_from_json(cls, json):
    kwargs = {'rid': json["id"],
              'data': json,
              'created_utc': json["created_utc"],
              'subreddit': json["subreddit"]}
    return cls(**kwargs)


def map_push_shift_object(name, tablename, searchname):
    class_ = type(name, (StoradjeBase,),
                  dict(__tablename__=tablename,
                       searchname=searchname,
                       crawl=classmethod(_classmethod_crawl),
                       from_json=classmethod(_classmethod_from_json),
                       add_by_id=classmethod(_add_by_id),
                       id=Column(Integer, primary_key=True),
                       rid=Column(String, nullable=False, unique=True),
                       data=Column(JSON, nullable=False),
                       created_utc=Column(Integer),
                       subreddit=Column(String, nullable=False),
                       __repr__=lambda self: (f"<{name}(rid={self.rid}, "
                                              f"id = {self.id})>")))
    return class_


PushShiftPost = map_push_shift_object(
    "PushShiftPost", "pushshift_posts", "search_submissions")

PushShiftComment = map_push_shift_object(
    "PushShiftComment", "pushshift_comments", "search_comments")
crawlers = {PushShiftPost: crawl_posts,
            PushShiftComment: crawl_comments}

